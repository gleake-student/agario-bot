#include "bot.h"
#include <math.h>
#include <stdlib.h>

double mex;
double mey;

int dist(const void *a, const void *b){
    double ax=((struct food *)a)->x;
    double ay=((struct food *)a)->y;
    double bx=((struct food *)b)->x;
    double by=((struct food *)b)->y;
    //Distance from me to a
    double dista= sqrt((mex-ax) * (mex-ax) + (mey-ay) * (mey - ay));
    
    //Distance from me to b
    double distb = sqrt((mex - bx) * (mex-bx) + (mey - by) * (mey -by));
    
    //Subtract dista-distb
    return dista-distb;
}

int distPlayer(const void *a, const void *b){
    double ax=((struct player *)a)->x;
    double ay=((struct player *)a)->y;
    double bx=((struct player *)b)->x;
    double by=((struct player *)b)->y;
    //Distance from me to a
    double dista= sqrt((mex-ax) * (mex-ax) + (mey-ay) * (mey - ay));
    
    //Distance from me to b
    double distb = sqrt((mex - bx) * (mex-bx) + (mey - by) * (mey -by));
    
    //Subtract dista-distb
    return dista-distb;
}

int distMass(const void *a, const void *b){
    double ax=((struct player *)a)->totalMass;
    double bx=((struct player *)b)->totalMass;
    
    /*
    //Distance from me to a
    double dista= sqrt((mex-ax) * (mex-ax) + (mey-ay) * (mey - ay));
    
    //Distance from me to b
    double distb = sqrt((mex - bx) * (mex-bx) + (mey - by) * (mey -by));
    */
    //Subtract dista-distb
    return ax - bx;
}

struct action playerMove(struct player me, 
                         struct player * players, int nplayers,
                         struct food * foods, int nfood,
                         struct cell * virii, int nvirii,
                         struct cell * mass, int nmass)
{
    struct action act;
    mex =me.x;
    mey = me.y;
    //sort foods array by distance
    qsort(foods, nfood, sizeof(struct food), dist);
    qsort(players, nplayers, sizeof (struct player), distPlayer);
    qsort(players, nplayers, sizeof (struct player), distMass);
    

    if (nplayers > 0){
        //if player is smaller than 80% of size then eat it 
        if (me.totalMass * .60 < players[0].totalMass){
            act.dx = (players[0].x - me.x) * 100;
            act.dy = (players[0].y -me.y) * 100;
            act.fire = 0;
            act.split = 0;
        }
        //run away if player is greater in size
     
        else if (me.totalMass * .85 < players[0].totalMass){
            act.dx = (players[0].x + me.x) * 100;
            act.dy = (players[0].y + me.y) * 100;
            act.fire = 0;
            act.split = 0;
        }
        //run away if player is greater in size
    }   
    /*
    else if (nfoods > 0 && nplayer == 0){
        //eat food
    }
    else if (nfoods < 0 && nplayer == 0){
        //go to place with food
    }
    else if (nfoods < 0 && nplayer >  0){
        
    }
    else if (nfood == 0 && nplayer > 0){
        
    }
    else if (nfood == 0 && nplayer < 0){
        
    }
    */
    else if (nfood > 0 && nplayers == 0){
        // Move to the nearest food
        act.dx = (foods[0].x - me.x) * 100;
        act.dy = (foods[0].y -me.y) * 100;
        act.fire = 0;
        act.split = 0;
    }
    else if(nplayers == 0 && nfood == 0){
        act.dx = (2500 - me.x) * 100;
        act.dy = (2500 - me.y) * 100;
        act.fire = 0;
        act.split = 0;
    }
    
    else if (me.totalMass > players[0].cells[0].mass * .35){
            act.dx = (players[0].x - me.x) * 100;
            act.dy = (players[0].y -me.y) * 100;
            act.fire = 1;
            act.split = 1;
        
    }
    
    return act;
} 